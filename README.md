## Homepage

https://walchinsky.gitlab.io/docs

---

## Running locally

To work locally with this project, you'll have to follow the steps below:

1. Install docker and docker-compose
2. Run `docker-compose up -d`
3. Access site at `localhost:8000`

Read more at [Mkdocs][documentation].

[documentation]: http://www.mkdocs.org